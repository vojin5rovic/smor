const width = window.screen.width;

if (width <= 768) {
  const height = Math.floor((500 * width) / 700);
  const canvas = document.getElementById("canvas");
  const nivoText = document.getElementsByClassName("nivoText")[0];

  const canvasHeight = height - (height % 8);
  const canvasWidth = width - (width % 11);

  canvas.width = canvasWidth * 2;
  canvas.height = canvasHeight * 2;

  canvas.style.height = canvasHeight + "px";
  canvas.style.width = canvasWidth + "px";

  nivoText.style.height = height + "px";
}
