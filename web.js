const canvas = document.getElementById("canvas");
const ctx = canvas.getContext("2d");

// Mutable state
let state = initialState();

// Position helpers
const x = (c) => Math.round((c * canvas.width) / state.cols);
const y = (r) => Math.round((r * canvas.height) / state.rows);

const nextLevel = () => {
  const level = parseInt(state.level) + 1;
  if (level !== levels.length) {
    const nxtLvl = levels[level] || {};
    state = {
      ...state,
      ...{ smorica: null },
      ...nxtLvl,
      level,
      smorActive: true,
      history: [],
    };
    draw();
    if (hasLocalStorage()) {
      localStorage.setItem(saveKey, level);
      document.getElementById("trenutniNivo").innerText = parseInt(level) + 1;
    }
  }
};

const getSmorImage = (lookingRight, smorActive) => {
  if (smorActive) {
    return !lookingRight ? smorLeftImage : smorRightImage;
  } else {
    return !lookingRight ? smorLeftGrayImage : smorRightGrayImage;
  }
};

const getSmoricaImage = (lookingRight, smorActive) => {
  if (!smorActive) {
    return !lookingRight ? smoricaLeftImage : smoricaRightImage;
  } else {
    return !lookingRight ? smoricaLeftGrayImage : smoricaRightGrayImage;
  }
};

// Game loop draw
const draw = () => {
  // draw level
  const switchButton = document.getElementById("switchButton");
  if (state.smorica) {
    switchButton.style.visibility = "visible";
  } else {
    switchButton.style.visibility = "hidden";
  }
  state.field.map((row, yIndex) => {
    row.map((field, xIndex) => {
      ctx.drawImage(assets[SKY], x(xIndex), y(yIndex), x(1), y(1));

      let fieldToDraw = field;

      if (Array.isArray(field)) {
        ctx.drawImage(assets[field[0]], x(xIndex), y(yIndex), x(1), y(1));
        fieldToDraw = field[1];
      }
      if (fieldToDraw === SMOR) {
        ctx.drawImage(
          getSmorImage(state.smorLookingRight, state.smorActive),
          x(xIndex),
          y(yIndex),
          x(1),
          y(1)
        );
      } else if (fieldToDraw === SMORICA) {
        ctx.drawImage(
          getSmoricaImage(state.smoricaLookingRight, state.smorActive),
          x(xIndex),
          y(yIndex),
          x(1),
          y(1)
        );
      } else {
        ctx.drawImage(assets[fieldToDraw], x(xIndex), y(yIndex), x(1), y(1));
      }
    });
  });

  // draw smor
  ctx.drawImage(
    getSmorImage(state.smorLookingRight, state.smorActive, state.moves[0]),
    x(state.smor.x),
    y(state.smor.y),
    x(1),
    y(1)
  );

  // draw smorica
  if (state.smorica) {
    ctx.drawImage(
      getSmoricaImage(
        state.smoricaLookingRight,
        state.smorActive,
        state.moves[0]
      ),
      x(state.smorica.x),
      y(state.smorica.y),
      x(1),
      y(1)
    );
  }

  if (!countMushrooms(state.field)) {
    nextLevel();
  }
};

// Game loop update
const step = async () => {
  state = await next(state);
  draw();
};

const nextStep = async (direction) => {
  const newState = enqueue(state, direction);
  if (newState !== state) {
    state = newState;
    await step();
  }
};

const toggleActive = () => {
  if (!state.smorica) {
    return;
  }
  const smor = state.smorActive ? state.smor : state.smorica;
  const otherSmor = !state.smorActive ? state.smor : state.smorica;
  const smorAsset = state.smorActive ? SMOR : SMORICA;
  const smorActive = !state.smorActive;
  state = {
    ...state,
    smorActive,
    history: [
      ...state.history,
      JSON.stringify({
        smor: state.smor,
        field: state.field,
        smorica: state.smorica,
        smorActive: state.smorActive,
      }),
    ],
  };
  state.field[smor.y][smor.x] =
    state.field[smor.y][smor.x] !== SKY
      ? [state.field[smor.y][smor.x], smorAsset]
      : smorAsset;
  state.field[otherSmor.y][otherSmor.x] = Array.isArray(
    state.field[otherSmor.y][otherSmor.x]
  )
    ? state.field[otherSmor.y][otherSmor.x][0]
    : SKY;
  draw();
};

const previousStep = () => {
  const previousState = state.history.pop();
  if (previousState) {
    state = { ...state, ...JSON.parse(previousState) };
  }
  draw();
};

const moveInDirection = async (direction) => {
  if (state.inputDisabled) {
    return;
  }
  switch (direction) {
    case "ArrowUp":
      await nextStep(NORTH);
      break;
    case "ArrowLeft":
      await nextStep(WEST);
      break;
    case "ArrowDown":
      await nextStep(SOUTH);
      break;
    case "ArrowRight":
      await nextStep(EAST);
      break;
    case "Shift":
      toggleActive();
      break;
    case "Enter":
      previousStep();
      break;
  }
};

let djukaInput = "";

// Key events
window.addEventListener("keydown", async (e) => {
  if (
    e.key === "d" ||
    e.key === "j" ||
    e.key === "u" ||
    e.key === "k" ||
    e.key === "a"
  ) {
    djukaInput += e.key;
    if (djukaInput.includes("djuka")) {
      smorLeftImage.src = `assets/djukaLeft.png`;
      smorRightImage.src = `assets/djukaRight.png`;
      draw();
    }
  } else {
    await moveInDirection(e.key);
  }
});

let counter = 0;

const imgs = [
  grayImage,
  skyImage,
  smorLeftImage,
  smorRightImage,
  smorLeftGrayImage,
  smorRightGrayImage,
  mushroomImage,
  boxImage,
  ladderImage,
  wallImage,
  icecreamImage,
  smoricaRightImage,
  smoricaLeftImage,
  smoricaRightGrayImage,
  smoricaLeftGrayImage,
  djukaLeftImage,
  djukaRightImage,
];

const incrementCounter = () => {
  counter++;
  if (counter === imgs.length) {
    draw();
  }
};

const toggleInfo = () => {
  const overlay = document.getElementById("overlay");
  overlay.style.display = overlay.style.display === "block" ? "none" : "block";
};

[].forEach.call(imgs, (img) => {
  if (img.complete) incrementCounter();
  else img.addEventListener("load", incrementCounter, false);
});
