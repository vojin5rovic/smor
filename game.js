const delay = (ms) => new Promise((res) => setTimeout(res, ms));

// Constants
const NORTH = { x: 0, y: -1 };
const SOUTH = { x: 0, y: 1 };
const EAST = { x: 1, y: 0 };
const WEST = { x: -1, y: 0 };

const ITEMS_THAT_FALL = [BOX, MUSHROOM, SMOR, SMORICA];

// Point operations
const pointEq = (p1, p2) => p1.x == p2.x && p1.y == p2.y;

const countMushrooms = (field) => {
  let count = 0;
  field.map((row) => {
    row.map((elm) => {
      if (elm === MUSHROOM || elm === ICECREAM) {
        count++;
      }
    });
  });
  return count;
};

// Booleans
const willPush = (state, move, smor) => {
  const nextField = state.field[smor.y][smor.x + move.x];
  return nextField !== BOX || state.field[smor.y][smor.x + move.x * 2] === SKY;
};

const isOtherSmorThere = (x, y, smor) => {
  if (!state.smorica) {
    return false;
  }
  const otherSmor = smor === state.smor ? state.smorica : state.smor;
  return pointEq({ x: x, y: y }, otherSmor);
};

const canMove = (state, move, smor) =>
  state.field[smor.y][smor.x + move.x] &&
  state.field[smor.y][smor.x + move.x] !== GRAY &&
  !isOtherSmorThere(smor.x + move.x, smor.y, smor);

const canClimb = (state, move, smor) =>
  ((state.field[smor.y][smor.x] === LADDER && state.field[smor.y + move.y]) ||
    (move.y === 1 && state.field[smor.y + move.y][smor.x] === LADDER)) &&
  state.field[smor.y + move.y][smor.x] !== GRAY &&
  state.field[smor.y + move.y][smor.x] !== BOX &&
  state.field[smor.y + move.y][smor.x] !== MUSHROOM &&
  state.field[smor.y + move.y][smor.x] !== ICECREAM &&
  state.field[smor.y + move.y][smor.x] !== WALL &&
  !isOtherSmorThere(smor.x, smor.y + move.y, smor);

const validMove = (move, state, smor) =>
  (move.y === 0 && canMove(state, move, smor) && willPush(state, move, smor)) ||
  (move.y !== 0 && canClimb(state, move, smor));

// Next values based on state
const nextSmor = (state, smor, isActive) => {
  if (isActive) {
    smor.x = smor.x + state.moves[0].x;
    smor.y = smor.y + state.moves[0].y;
  }
  return smor;
};

const fallOnAfterItem = (field, x, y, smor) => {
  let prev = 1;
  while (field[y - prev] && ITEMS_THAT_FALL.includes(field[y - prev][x])) {
    draw();
    if (field[y - prev][x] === SMOR || field[y - prev][x] === SMORICA) {
      const otherSmor = smor === state.smor ? state.smorica : state.smor;
      otherSmor.x = x;
      otherSmor.y = y - prev + 1;
    }
    field[y - prev + 1][x] = field[y - prev][x];
    prev++;
  }
  field[y - prev + 1][x] = SKY;
};

const updateFieldFalls = async (state, smor) => {
  const field = state.field;
  for (let i = 0; i < field.length - 1; i++) {
    for (let j = 0; j < field[i].length; j++) {
      const canFall = ITEMS_THAT_FALL.includes(field[i][j]);
      const willFall =
        field[i + 1][j] === SKY && !pointEq(smor, { x: j, y: i + 1 });
      if (canFall && willFall) {
        const element = field[i][j];
        draw();
        state.inputDisabled = true;
        await delay(100);
        state.inputDisabled = false;
        if (element === SMOR || element === SMORICA) {
          const otherSmor = smor === state.smor ? state.smorica : state.smor;
          otherSmor.x = j;
          otherSmor.y = i + 1;
        }
        field[i + 1][j] = element;
        fallOnAfterItem(field, j, i, smor);
      }
    }
  }
  return field;
};

const updateSmorFalls = async (field, smor) => {
  while (1) {
    if (
      field[smor.y + 1] &&
      field[smor.y + 1][smor.x] === SKY &&
      field[smor.y][smor.x] !== LADDER &&
      !isOtherSmorThere(smor.x, smor.y + 1, smor)
    ) {
      if (field[smor.y][smor.x] !== SKY) {
        field[smor.y][smor.x] = SKY;
      }
      draw();
      state.inputDisabled = true;
      await delay(100);
      state.inputDisabled = false;
      smor.y = smor.y + 1;
      fallOnAfterItem(field, smor.x, smor.y - 1, smor);
    } else break;
  }
  return smor;
};

const nextField = async (state, smor) => {
  // eats mushroom
  if (
    state.field[smor.y][smor.x] === MUSHROOM ||
    state.field[smor.y][smor.x] === ICECREAM
  ) {
    state.field[smor.y][smor.x] = SKY;
  }
  // pushes box
  if (state.field[smor.y][smor.x] === BOX) {
    state.field[smor.y][smor.x] = SKY;
    state.field[smor.y][smor.x + state.moves[0].x] = BOX;
    smor.x = smor.x - state.moves[0].x;
  }
  // destroyed wall
  if (state.field[smor.y][smor.x] === WALL) {
    state.field[smor.y][smor.x] = SKY;
  }

  return await updateFieldFalls(state, smor);
};

// Initial state
const initialState = () => ({
  ...levels[currentLevel],
  inputDisabled: false,
  cols: 11,
  rows: 8,
  moves: [EAST],
  level: currentLevel,
  history: [],
  smorActive: true,
  smorLookingRight: true,
  smoricaLookingRight: true,
});

const next = async (state) => {
  const nextMove = state.moves[0];
  const lookingRight = nextMove.x !== -1;

  const smor = nextSmor(state, state.smor, state.smorActive);
  const smorica = nextSmor(state, state.smorica, !state.smorActive);
  await updateSmorFalls(state.field, state.smorActive ? smor : smorica);

  return {
    ...state,
    field: await nextField(state, state.smorActive ? smor : smorica),
    smor,
    smorica,
    smorLookingRight: state.smorActive ? lookingRight : state.smorLookingRight,
    smoricaLookingRight: !state.smorActive
      ? lookingRight
      : state.smoricaLookingRight,
  };
};

const enqueue = (state, move) =>
  validMove(move, state, state.smorActive ? state.smor : state.smorica)
    ? {
        ...state,
        moves: [move].concat(state.moves),
        history: [
          ...state.history,
          JSON.stringify({
            smor: state.smor,
            field: state.field,
            smorica: state.smorica,
            smorActive: state.smorActive,
            smorLookingRight: state.smorLookingRight,
            smoricaLookingRight: state.smoricaLookingRight,
          }),
        ],
      }
    : state;
