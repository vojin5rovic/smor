const saveKey = "smorLevel";

const hasLocalStorage = () => {
  try {
    localStorage.getItem(saveKey);
    return true;
  } catch (e) {
    return false;
  }
};

const currentLevel = (hasLocalStorage() && localStorage.getItem(saveKey)) || 0;

document.getElementById("trenutniNivo").innerText = parseInt(currentLevel) + 1;
